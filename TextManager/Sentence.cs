﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TextManager
{
    internal class Sentence
    {
        private List<Word> _words;

        /// <summary>
        /// Конструктор. Создаёт предложение из слов.
        /// </summary>
        /// <param name="words">Список слов (классов Word)</param>
        public Sentence(List<Word> words)
        {
            _words = words;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public Sentence() { _words = new List<Word>(); }

        /// <summary>
        /// Список слов (список классов Word)
        /// </summary>
        public List<Word> Words
        {
            get { return _words; }
            set { _words = value; }
        }

        /// <summary>
        /// Добавляет слово в предложение.
        /// </summary>
        /// <param name="text">Текст слова</param>
        public void AddWord(String text)
        {
            if (text.Any())
                _words.Add(new Word(text));
        }

        /// <summary>
        /// Возвращает количество слов в предложении
        /// </summary>
        /// <returns>Количество слов в предложении</returns>
        public int GetWordCount()
        {
            return _words.Count();
        }

        /// <summary>
        /// Преобразует текст в список классов Word. Записывает в поле _words
        /// </summary>
        /// <param name="text">Текст для парсинга</param>
        public void ParceText(String text)
        {
            _words.Clear();
            //Remove all non letter and number symbols
            var newText = Regex.Replace(text, "[-.?!)(,:]", " ");
            //Remove spaces ( from "  " to " " )
            while (newText.IndexOf("  ", StringComparison.Ordinal) != -1) newText = newText.Replace("  ", " ");

            string[] wordsArray = newText.Split(' ');
            foreach (string word in wordsArray)
            {
                AddWord(word);
            }
        }

        /// <summary>
        /// Преобразует предложение в строку
        /// </summary>
        /// <returns>Строка, содержащая все слова предложения</returns>
        public String ToStringA()
        {
            String result = null;
            foreach (Word word in _words)
            {
                result += word.Text + " ";
            }
            return result;
        }

        /// <summary>
        /// Заменить N-ную букву в каждом слове предложения
        /// </summary>
        /// <param name="n">Позиция буквы (по номеру, начиная с 1)</param>
        /// <param name="z">Буква, на которую необходимо заменить</param>
        /// <returns>Возвращает количество замененных букв.</returns>
        public int ReplaceLetter(int n, char z)
        {
            int result = 0;
            foreach (Word word in _words)
            {
                if (word.ReplaceLetter(n, z)) result++;
            }
            return result;
        }
    }
}