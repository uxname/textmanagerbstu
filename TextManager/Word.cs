﻿using System;
using System.Linq;

namespace TextManager
{
    internal class Word
    {
        private String _text;

        /// <summary>
        /// Текст, который содержит слово, имеет getter и setter,
        /// при использовании setter происходит проверка текста.
        /// При присутствии посторонних (не буквы или цифры) происходит
        /// исключение.
        /// </summary>
        public String Text
        {
            get { return _text; }
            set
            {
                if (IsWord(value))
                    _text = value;
                else throw new Exception("Word: Text contain non letters and digits");
            }
        }

        /// <summary>
        /// Проверка текста на сторонние символы.
        /// Допускаются только буквы и цифры.
        /// </summary>
        /// <param name="sentence">Текст для проверки</param>
        /// <returns>True - если посторонних символов нет. False - если присутствуют посторонние символы</returns>
        protected static bool IsWord(String text)
        {
            return text.All(char.IsLetter) && text.Any();
        }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="sentence">Текст, который будет содержать слово</param>
        public Word(String text)
        {
            if (IsWord(text) == false)
                if (text.Any() == false) throw new Exception("Word: Word is not plain sentence");
            this.Text = text;
        }

        /// <summary>
        /// Заменить N-ную букву в слове
        /// </summary>
        /// <param name="n">Позиция буквы (по номеру, начиная с 1)</param>
        /// <param name="z">Буква, на которую необходимо заменить</param>
        /// <returns>Возвращает true в случае успешной замены. Возвращает false в случае невозможности замены.</returns>
        public bool ReplaceLetter(int n, char z)
        {
            if (n <= 0) return false;
            if (n > _text.Count()) return false;
            //TODO Добавить, если нужно, проверку, что бы символ был ТООЛЬКО буквой

            Text = Text.Remove(n - 1, 1).Insert(n - 1, z.ToString());
            return true;
        }
    }
}