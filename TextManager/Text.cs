﻿using System;
using System.Collections.Generic;

namespace TextManager
{
    internal class Text
    {
        private List<Sentence> _sentences;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="sentences">Список предложений (классов Sentence) которые будет содержать текст</param>
        public Text(List<Sentence> sentences)
        {
            _sentences = sentences;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public Text()
        {
            _sentences = new List<Sentence>();
        }

        /// <summary>
        /// Список предложений (классов Sentence)
        /// </summary>
        public List<Sentence> Sentences
        {
            get { return _sentences; }
            set { _sentences = value; }
        }

        /// <summary>
        /// Добавляет предложение в коллекцию
        /// </summary>
        /// <param name="sentence">Предложение</param>
        public void AddSentence(Sentence sentence)
        {
            Sentence tmpSentence = new Sentence(sentence.Words);
            if (sentence.GetWordCount() > 0)
                _sentences.Add(tmpSentence);
            //TODO Добавить перегрузку этого метода для случая когда на вход передаётся текст
        }

        /// <summary>
        /// Преобразует текст в список классов Sentence. Записывает в поле _sentences
        /// </summary>
        /// <param name="text">Текст для парсинга</param>
        public void ParseText(String text)
        {
            _sentences.Clear();
            string[] sentenceArray = text.Split('.');

            foreach (String item in sentenceArray)
            {
                Sentence tmpSentence = new Sentence();
                tmpSentence.ParceText(item);
                AddSentence(tmpSentence);
            }
        }

        /// <summary>
        /// Преобразует текст в строку
        /// </summary>
        /// <returns>Строка, содержащая все слова текста</returns>
        public String ToStringA()
        {
            String result = null;
            foreach (Sentence snt in _sentences)
            {
                result += snt.ToStringA();
            }
            return result;
        }

        /// <summary>
        /// Заменить N-ную букву в каждом слове предложения
        /// </summary>
        /// <param name="n">Позиция буквы (по номеру, начиная с 1)</param>
        /// <param name="z">Буква, на которую необходимо заменить</param>
        /// <returns>Возвращает количество замененных букв.</returns>
        public int ReplaceLetter(int n, char z)
        {
            int result = 0;
            foreach (Sentence sentence in _sentences)
            {
                result += sentence.ReplaceLetter(n, z);
            }
            return result;
        }
    }
}